import java.io.IOException;
import java.util.HashMap;
import java.lang.Math;

import org.json.JSONObject;
import org.json.JSONException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.GenericOptionsParser;

public class Indexer {
    public static class IndexerMapper
            extends Mapper<LongWritable, Text, IntWritable, Text> {

        @Override
        public void map(LongWritable key, Text value, Context context
                        ) throws IOException, InterruptedException {
            JSONObject record = null;
            IntWritable docId = null;
            String[] words = null;

            try {
                record = new JSONObject(value.toString());
                docId = new IntWritable(record.getInt("id"));
                words = record.getString("text").toLowerCase()
                                       .split("[^a-zA-Z']+");
            } catch (JSONException e) {
                System.exit(3);
            }

            HashMap<String, Integer> wordCount = new HashMap<>(
                Math.round((float) Math.sqrt(words.length))
            );
            for (String word : words) {
                if (word.length() > 0) {
                    if (wordCount.containsKey(word)) {
                        wordCount.put(word, wordCount.get(word) + 1);
                    } else {
                        wordCount.put(word, 1);
                    }
                }
            }

            for (String word : wordCount.keySet()) {
                context.write(
                    docId,
                    new Text(word + "=" + wordCount.get(word).toString())
                );
            }
        }
    }

    public static class IndexerReducer
            extends Reducer<IntWritable, Text, IntWritable, Text> {

        private IntWritable docId = new IntWritable(0);

        private HashMap<String, Integer> wordIdf = new HashMap<>();

        private HashMap<String, Integer> termFreq = new HashMap<>();

        @Override
        public void reduce(IntWritable key, Iterable<Text> values,
                           Context context
                           ) throws IOException, InterruptedException {
            for (Text value : values) {
                String[] pair = value.toString().split("=");
                String word = pair[0];
                Integer count = Integer.parseInt(pair[1]);

                if (wordIdf.containsKey(word)) {
                    wordIdf.put(word, wordIdf.get(word) + 1);
                } else {
                    wordIdf.put(word, 1);
                }

                termFreq.put(key.toString() + ":" + word, count);
            }
        }

        @Override
        public void cleanup(Context context
                            ) throws IOException, InterruptedException {
            for (String term : termFreq.keySet()) {
                String[] pair = term.split(":");
                docId.set(Integer.parseInt(pair[0]));
                String word = pair[1];

                context.write(
                    docId,
                    new Text(word + "="
                             + termFreq.get(term).toString() + "/"
                             + wordIdf.get(word).toString())
                );
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration configuration = new Configuration();
        GenericOptionsParser optionParser = new GenericOptionsParser(configuration, args);

        String[] remainingArgs = optionParser.getRemainingArgs();
        if (remainingArgs.length != 2) {
            System.err.println("Usage: hadoop jar indexer.jar Indexer /path/to/input/folder /path/to/output/folder");
            System.exit(2);
        }

        Job job = Job.getInstance(configuration, "Indexing");
        job.setJarByClass(Indexer.class);
        job.setMapperClass(IndexerMapper.class);
        job.setReducerClass(IndexerReducer.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);

        TextInputFormat.addInputPath(job, new Path(remainingArgs[0]));
        TextOutputFormat.setOutputPath(job, new Path(remainingArgs[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
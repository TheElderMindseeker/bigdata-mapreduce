import java.io.IOException;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Collections;
import java.lang.Math;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.GenericOptionsParser;

public class Query {
    public static class QueryMapper
            extends Mapper<LongWritable, Text, IntWritable, FloatWritable> {

        private HashMap<String, Integer> queryVector = new HashMap<>();

        private IntWritable documentId = new IntWritable();

        private FloatWritable term = new FloatWritable();

        @Override
        public void setup(Context context) {
            Configuration conf = context.getConfiguration();
            String query = conf.get("query");

            String[] words = query.toLowerCase().split("[^a-zA-Z']+");

            for (String word : words) {
                if (word.length() > 0) {
                    if (queryVector.containsKey(word)) {
                        queryVector.put(word, queryVector.get(word) + 1);
                    } else {
                        queryVector.put(word, 1);
                    }
                }
            }
        }

        @Override
        public void map(LongWritable key, Text value, Context context
                        ) throws IOException, InterruptedException {
            String[] split = value.toString().split("[\t=/]");
            Integer docId = Integer.parseInt(split[0]);
            String word = split[1];
            Integer termFreq = Integer.parseInt(split[2]);
            Integer wordIdf = Integer.parseInt(split[3]);

            if (queryVector.containsKey(word)) {
                term.set(
                    (float) termFreq / wordIdf
                    * (float) queryVector.get(word) / wordIdf
                );
                documentId.set(docId);

                context.write(documentId, term);
            }
        }
    }

    public static class QueryReducer
            extends Reducer<IntWritable, FloatWritable, IntWritable, FloatWritable> {

        private ArrayList<AbstractMap.SimpleEntry<IntWritable, Float>> output
            = new ArrayList<>();

        private FloatWritable result = new FloatWritable();

        @Override
        public void reduce(IntWritable key, Iterable<FloatWritable> values,
                           Context context
                           ) throws IOException, InterruptedException {
            float sum = .0f;

            for (FloatWritable term : values) {
                sum += term.get();
            }

            output.add(new AbstractMap.SimpleEntry(new IntWritable(key.get()),
                                                   new Float(sum)));
        }

        @Override
        public void cleanup(Context context
                            ) throws IOException, InterruptedException {
            Collections.sort(output,
                new Comparator<AbstractMap.SimpleEntry<IntWritable, Float>>() {
                    @Override
                    public int compare(
                        AbstractMap.SimpleEntry<IntWritable, Float> se1,
                        AbstractMap.SimpleEntry<IntWritable, Float> se2
                    ) {
                        return -se1.getValue().compareTo(se2.getValue());
                    }
                });

            for (AbstractMap.SimpleEntry<IntWritable, Float> entry : output) {
                context.write(
                    entry.getKey(),
                    new FloatWritable(entry.getValue())
                );
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration configuration = new Configuration();
        GenericOptionsParser optionParser = new GenericOptionsParser(configuration, args);

        String[] remainingArgs = optionParser.getRemainingArgs();
        if (remainingArgs.length != 3) {
            System.err.println("Usage: hadoop jar query.jar /path/to/input/folder /path/to/output/folder Query <query_string>");
            System.exit(2);
        }

        configuration.set("query", remainingArgs[2]);

        Job job = Job.getInstance(configuration, "Querying");
        job.setJarByClass(Query.class);
        job.setMapperClass(QueryMapper.class);
        job.setReducerClass(QueryReducer.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(FloatWritable.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(FloatWritable.class);

        TextInputFormat.addInputPath(job, new Path(remainingArgs[0]));
        TextOutputFormat.setOutputPath(job, new Path(remainingArgs[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}